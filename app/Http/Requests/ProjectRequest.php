<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();

    }

    public function onStore()
    {
        return [
            'title' => 'required|string',
            'content' => 'required|string',
            'photos' => 'required|array|min:1',
            'photos.*' => 'file',
            'customer' => 'required|string',
            'end_date' => 'required|date',
            'meta_title' => 'sometimes|nullable|string',
            'meta_description' => 'sometimes|nullable|string',
            'meta_keywords' => 'sometimes|nullable|string',
        ];

    }//end of onStore function

    public function onUpdate()
    {
        return [
            'title' => 'required|string',
            'photos' => 'sometimes|nullable|array|min:1',
            'photos.*' => 'file',
            'content' => 'required|string',
            'customer' => 'required|string',
            'end_date' => 'required|date',
            'meta_title' => 'sometimes|nullable|string',
            'meta_description' => 'sometimes|nullable|string',
            'meta_keywords' => 'sometimes|nullable|string',
        ];

    }//end of onUpdate function
}
