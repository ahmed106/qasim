<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SliderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();

    }

    public function onStore()
    {
        return [
            'title' => 'required|string|max:100',
            'content' => 'required|string|max:200',
            'meta_title' => 'sometimes|nullable|string',
            'meta_description' => 'sometimes|nullable|string',
            'meta_keywords' => 'sometimes|nullable|string',
            'link' => 'sometime|nullable|url',
            'photo' => 'required|nullable|file',
        ];

    }//end of onStore function

    public function onUpdate()
    {
        return [
            'title' => 'required|string|max:100',
            'content' => 'required|string|max:200',
            'meta_title' => 'sometimes|nullable|string',
            'meta_description' => 'sometimes|nullable|string',
            'meta_keywords' => 'sometimes|nullable|string',
            'link' => 'sometime|nullable|url',
            'photo' => 'sometimes|nullable|file',

        ];

    }//end of onUpdate function


}
