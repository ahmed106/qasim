<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BlogRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function rules()
    {

        return request()->isMethod('put') || request()->isMethod('patch') ? $this->onUpdate() : $this->onStore();

    }

    public function onStore()
    {
        return [
            'title' => 'required|string',
            'content' => 'required|string',
            'photo' => 'required|file',
            'file' => 'required|file',
            'meta_title' => 'sometimes|nullable|string',
            'meta_description' => 'sometimes|nullable|string',
            'meta_keywords' => 'sometimes|nullable|string',
        ];

    }//end of onStore function

    public function onUpdate()
    {
        return [
            'title' => 'required|string',
            'photo' => 'sometimes|nullable|file',
            'file' => 'sometimes|nullable|file',
            'content' => 'required|string',
            'meta_title' => 'sometimes|nullable|string',
            'meta_description' => 'sometimes|nullable|string',
            'meta_keywords' => 'sometimes|nullable|string',
        ];

    }//end of onUpdate function
}
