<?php

namespace App\Http\Controllers;

use App\Helper\UploadTrait;
use App\Http\Requests\SliderRequest;
use App\Models\Slider;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class SliderController extends Controller
{
    use UploadTrait;

    public function index()
    {

        return view('dashboard.sliders.index');
    }


    public function create()
    {
        return view('dashboard.sliders.create');
    }

    public function data()
    {
        $model = 'sliders';
        $sliders = Slider::with('photo')->get();
        return DataTables::of($sliders)
            ->addColumn('check_item', function ($raw) {
                return '<input type="checkbox" name="items[]" class="check_item" value="' . $raw->id . '">';
            })
            ->addColumn('photo', function ($raw) {
                return '<img width="100" height="80"  src="' . $raw->image . '" >';
            })
            ->addcolumn('actions', function ($raw) use ($model) {

                return view('dashboard.includes.actions', compact('raw', 'model'));
            })
            ->rawColumns(['photo' => 'photo', 'check_item' => 'check_item'])
            ->make(true);

    }//end of data function


    public function store(SliderRequest $request)
    {

        $data = $request->validated();
        unset($data['photo']);
        $slider = Slider::create($data);

        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'sliders');
            $slider->photo()->create([
                'src' => $photo,
                'type' => 'slider',
            ]);
        };

        return redirect()->route('sliders.index')->with('success', 'تم إضافه البيانات بنجاح');

    }


    public function edit(Slider $slider)
    {
        return view('dashboard.sliders.edit', compact('slider'));
    }

    public function update(SliderRequest $request, Slider $slider)
    {
        $data = $request->validated();
        unset($data['photo']);
        $slider->update($data);
        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'sliders');
            $slider->photo ? $this->deleteOldPhoto('images/sliders/', $slider->photo->src)
                & $slider->photo()->update(['src' => $photo]) : $slider->photo()->create(['src' => $photo, 'type' => 'slider']);
        }

        return redirect()->route('sliders.index')->with('success', 'تم تعديل البيانات بنجاح');


    }


    public function destroy(Slider $slider)
    {
        $slider->photo ? $this->deleteOldPhoto('images/sliders/', $slider->photo->src) & $slider->photo()->delete() : '';
        Slider::destroy($slider->id);
        return redirect()->route('sliders.index')->with('success', 'تم حذف البيانات بنجاح');
    }

    public function bulkDelete(Request $request)
    {
        foreach ($request->items as $item) {
            $slider = Slider::findOrFail($item);
            $this->destroy($slider);
        }
        Slider::destroy($request->items);
        return redirect()->route('sliders.index')->with('success', 'تم حذف البيانات بنجاح');


    }//end of bulkDelete function


}
