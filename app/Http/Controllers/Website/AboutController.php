<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Customer;
use App\Models\Page;

class AboutController extends Controller
{
    public function index()
    {
        $page_name = Page::where('url','/about')->first()->name;

        $about = About::with('photo')->first();
        $customers = Customer::with('photo')->orderBy('id', 'desc')->get();

        return view('website.pages.about.index', compact('about', 'customers','page_name'));
    }//end of index function
}
