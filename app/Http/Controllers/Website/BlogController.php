<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Customer;
use App\Models\Page;

class BlogController extends Controller
{
    public function index()
    {

        $page_name = Page::where('url','/blogs')->first()->name;

        $blogs = Blog::with('photo')->get();
        $customers = Customer::with('photo')->get();


        return view('website.pages.blogs.index', compact('blogs', 'customers','page_name'));

    }//end of index function

    public function show($id)
    {
        $page_name = Page::where('url','/blogs')->first()->name;
        $blog = Blog::with('photo')->findOrFail($id);
        $blogs = Blog::with('photo')->where('id', '!=', $id)->get();
        $nextBlog = Blog::where('id', '>', $id)->with('photo')->first();
        $prevBlog = Blog::where('id', '<', $id)->with('photo')->first();

        return view('website.pages.blogs.show', compact('blog', 'prevBlog', 'nextBlog', 'blogs','page_name'));

    }//end of show function
}
