<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\Models\Service;

class ServiceController extends Controller
{
    public function index()
    {
        $page_name = Page::where('url','/services')->first()->name;
        return view('website.pages.services.index',compact('page_name'));
    }//end of index function

    public function show($id)
    {
        $page_name = Page::where('url','/services')->first()->name;
        $service = Service::findOrFail($id);
        return view('website.pages.services.show', compact('service','page_name'));
    }//end of show function
}
