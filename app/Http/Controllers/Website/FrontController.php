<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Blog;
use App\Models\Customer;
use App\Models\Page;
use App\Models\Project;
use App\Models\Section;
use App\Models\Service;
use App\Models\Slider;

class FrontController extends Controller
{
    public function index()
    {


        $home_service_section = Section::where('section_name','home_service_section')->first();
        $home_vision_section = Section::where('section_name','home_vision_section')->first();

        $sliders = Slider::with('photo')->orderBy('id', 'desc')->get();
        $services = Service::with('photo')->get();
        $projects = Project::with('photos')->get();
        $customers = Customer::with('photo')->get();
        $blogs = Blog::with('photo')->get();
        $about = About::with('photo')->first();
        return view('home', compact('sliders', 'services', 'projects', 'customers', 'blogs', 'about','home_service_section','home_vision_section'));

    }//end of index function
}
