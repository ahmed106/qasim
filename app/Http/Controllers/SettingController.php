<?php

namespace App\Http\Controllers;

use App\Helper\UploadTrait;
use App\Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    use UploadTrait;

    public function index()
    {
        $setting = \App\Models\Setting::first();


        return view('dashboard.settings.index', compact('setting'));

    }//end of index function

    public function store(Request $request)
    {

        $data = $request->validate([
            'website_name' => 'required|string',
            'website_email' => 'required|email',
            'website_address' => 'required|string',
            'website_phone' => 'sometimes|nullable',
            'website_facebook' => 'sometimes|nullable',
            'website_twitter' => 'sometimes|nullable',
            'website_linked_in' => 'sometimes|nullable',
            'meta_title' => 'sometimes|nullable|string',
            'meta_description' => 'sometimes|nullable|string',
            'meta_keywords' => 'sometimes|nullable|string',
            'map' => 'sometimes|nullable',
            'header'=>'sometimes|nullable',
            'footer'=>'sometimes|nullable',
        ]);

        $setting = \App\Models\Setting::first();
        if (!$setting) {
            $setting = \App\Models\Setting::create($data);
        } else {

            $setting->update($data);
        }
        if ($request->hasFile('logo')) {

            $setting->logo ? $this->deleteOldPhoto('images/settings', $setting->logo->src) & $setting->logo()->delete() : '';


            $logo = $this->upload($request->logo, 'settings');
            $setting->logo()->create([
                'src' => $logo,
                'type' => 'setting'
            ]);
        };

        return redirect()->route('settings.index')->with('success', 'تم تعديل البيانات بنجاح');

    }//end of store function
}
