<?php

namespace App\Http\Controllers;

use App\Models\ContactUs;
use Illuminate\Http\Request;

class ContactUsController extends Controller
{
    public function index()
    {

        return view('dashboard.contact_us.index');

    }//end of index function

    public function data()
    {
        $model = 'contact-us';
        $contact_us = ContactUs::get();


        return \Yajra\DataTables\DataTables::of($contact_us)
            ->addColumn('check_item', function ($raw) {
                return '<input type="checkbox" name="items[]" class="check_item" value="' . $raw->id . '">';
            })
            ->addcolumn('actions', function ($raw) use ($model) {

                return view('dashboard.includes.actions', compact('raw', 'model'));
            })
            ->rawColumns(['check_item' => 'check_item'])
            ->make(true);


    }//end of  function

    public function edit($id)
    {
        $contact_us = ContactUs::findOrFail($id);

        return view('dashboard.contact_us.edit', compact('contact_us'));

    }//end of edit function

    public function destroy($id)
    {


        ContactUs::destroy($id);
        return redirect()->route('contact-us.index')->with('success', 'تم حذف البيانات بنجاح');
    }

    public function bulkDelete(Request $request)
    {
        foreach ($request->items as $item) {
            $contact_us = ContactUs::findOrFail($item);
            $this->destroy($contact_us);
        }
        ContactUs::destroy($request->items);
        return redirect()->route('contact-us.index')->with('success', 'تم حذف البيانات بنجاح');


    }//end of bulkDelete function
}
