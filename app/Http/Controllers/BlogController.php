<?php

namespace App\Http\Controllers;

use App\Helper\UploadTrait;
use App\Http\Requests\BlogRequest;
use App\Models\Blog;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class BlogController extends Controller
{
    use UploadTrait;

    public function index()
    {

        return view('dashboard.blogs.index');
    }


    public function create()
    {
        return view('dashboard.blogs.create');
    }

    public function data()
    {
        $model = 'blogs';
        $blogs = Blog::with('photo')->get();
        return DataTables::of($blogs)
            ->addColumn('check_item', function ($raw) {
                return '<input type="checkbox" name="items[]" class="check_item" value="' . $raw->id . '">';
            })
            ->addColumn('photo', function ($raw) {
                return '<img width="100" height="80"  src="' . $raw->image . '" >';
            })
            ->addcolumn('actions', function ($raw) use ($model) {

                return view('dashboard.includes.actions', compact('raw', 'model'));
            })
            ->rawColumns(['photo' => 'photo', 'check_item' => 'check_item'])
            ->make(true);

    }//end of data function


    public function store(BlogRequest $request)
    {

        $data = $request->validated();
        unset($data['photo']);

        $data['created_by'] = auth()->id();
        if ($request->hasFile('file')) {
            $file = $this->upload($request->file, 'files');
           $data['file'] = $file;
        };
        $blog = Blog::create($data);

        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'blogs');
            $blog->photo()->create([
                'src' => $photo,
                'type' => 'Blog',
            ]);
        };



        return redirect()->route('blogs.index')->with('success', 'تم إضافه البيانات بنجاح');

    }


    public function edit(Blog $blog)
    {
        return view('dashboard.blogs.edit', compact('blog'));
    }

    public function update(BlogRequest $request, Blog $blog)
    {
        $data = $request->validated();
        unset($data['photo']);
        if ($request->hasFile('file')) {
            $this->deleteOldPhoto('images/files/',$blog->file);
            $file = $this->upload($request->file, 'files');
            $data['file'] = $file;
        };

        $blog->update($data);
        if ($request->hasFile('photo')) {
            $photo = $this->upload($request->photo, 'blogs');
            $blog->photo ? $this->deleteOldPhoto('images/blogs/', $blog->photo->src)
                & $blog->photo()->update(['src' => $photo]) : $blog->photo()->create(['src' => $photo, 'type' => 'Blog']);
        }

        return redirect()->route('blogs.index')->with('success', 'تم تعديل البيانات بنجاح');


    }


    public function destroy(Blog $blog)
    {
        $blog->photo ? $this->deleteOldPhoto('images/blogs/', $blog->photo->src) & $blog->photo()->delete() : '';
        $this->deleteOldPhoto('images/files/',$blog->file);
        Blog::destroy($blog->id);
        return redirect()->route('blogs.index')->with('success', 'تم حذف البيانات بنجاح');
    }

    public function bulkDelete(Request $request)
    {
        foreach ($request->items as $item) {
            $blog = Blog::findOrFail($item);
            $this->destroy($blog);
        }
        Blog::destroy($request->items);
        return redirect()->route('blogs.index')->with('success', 'تم حذف البيانات بنجاح');


    }//end of bulkDelete function
}
