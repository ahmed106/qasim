<?php

namespace App\Http\Controllers;

use App\Helper\UploadTrait;
use App\Models\Blog;
use App\Models\Customer;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class HomeController extends Controller
{
    use UploadTrait;

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $data['blogs_count'] = DB::table('blogs')->count();
        $data['projects_count'] = DB::table('projects')->count();
        $data['services_count'] = DB::table('services')->count();
        $data['customers_count'] = DB::table('customers')->count();


        $from = Carbon::now()->subMonths(6);
        $to = Carbon::now()->addMonths(6);
        $monthsList = [];
        $blogsList = [];
        $durations = $from->diffInMonths($to);

        for ($i = 1; $i <= $durations; $i++) {
            array_unshift($monthsList, $to->format('M'));
            array_unshift($blogsList, Blog::whereMonth('created_at', $to->format('m'))->count());
            $to->subMonth();
        }
        $adminsList = Customer::count();

        $adminsList = json_encode($adminsList, JSON_UNESCAPED_UNICODE);
        $blogsList = json_encode($blogsList, JSON_UNESCAPED_UNICODE);
        $monthsList = json_encode($monthsList, JSON_UNESCAPED_UNICODE);


        return view('dashboard.index', compact('data', 'monthsList', 'blogsList', 'adminsList'));
    }

    public function profile()
    {
        return view('dashboard.auth.profile');
    }//end of profile function

    public function editProfile(Request $request)
    {
        $user_id = \auth()->user()->id;
        $author = User::where('id', $user_id)->first();
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|email',
            'photo' => 'sometimes|nullable|file'
        ]);
        $data = $request->except('_token', 'photo', 'password');

        if ($request->hasFile('photo')) {
            $this->deleteOldPhoto('images/profile', $author->photo);
            $photo = $this->upload($request->photo, 'profile');

            $data['photo'] = $photo;
        }


        if ($request->password) {
            $data['password'] = Hash::make($request->password);

        } else {
            $data['password'] = \auth()->user()->password;
        }


        $author->update($data);
        return redirect()->back()->with('success', 'تم تعديل البيانات بنجاح');


    }//end of editProfile function
}
