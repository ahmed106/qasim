<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $table = 'projects';
    protected $guarded = [];
    protected $with = 'photos';

    public function photos()
    {
        return $this->morphMany(Photo::class, 'photoable');
    }//end of  function
}
