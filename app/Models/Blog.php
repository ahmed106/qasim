<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $table = 'blogs';
    protected $guarded = [];

    protected $appends = ['image'];

    public function creator()
    {

        return $this->belongsTo(User::class, 'created_by');

    }//end of user function

    public function photo()
    {

        return $this->morphOne(Photo::class, 'photoable');

    }//end of photo function

    public function getImageAttribute()
    {

        if ($this->photo == '') {
            return asset('default.svg');
        }

        return asset('images/blogs/' . $this->photo->src);


    }//end of getImageAttribute function
}
