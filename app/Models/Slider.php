<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'sliders';

    protected $guarded = [];

    protected $appends = ['image'];

    public function photo()
    {

        return $this->morphOne(Photo::class, 'photoable');

    }//end of photo function

    public function getImageAttribute()
    {
        if ($this->photo == '') {
            return asset('default.svg');
        } else {
            return asset('images/sliders/' . $this->photo->src);
        }

    }//end of getPhotoAttribute function


}
