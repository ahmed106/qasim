<?php

namespace App\Providers;

use App\Models\About;
use App\Models\Blog;
use App\Models\Page;
use App\Models\Service;
use App\Models\Setting;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('services') || Schema::hasTable('settings')) {
            $services = Service::select(['title', 'id', 'content'])->get();
            $setting = Setting::first();
            $about = About::with('photo')->first();
            $blogs = Blog::with('photo')->get();
            $pages = Page::get();
            View::composer('*', function ($view) use ($services, $setting, $blogs, $about,$pages) {
                $view->with(['services' => $services]);
                $view->with(['setting' => $setting]);
                $view->with(['about' => $about]);
                $view->with(['blogs' => $blogs]);
                $view->with(['pages' => $pages]);
            });

        }
        Schema::defaultStringLength(191);
    }
}
