<!DOCTYPE html>
<html lang="en" id="demo">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
    <meta name="description" content="Dashboard">
    <link rel="icon" href="{{asset('assets/dashboard')}}//img/logo.svg" type="image/x-icon">
    <title>Dashboard</title>
    <link id="style" href="{{asset('assets/dashboard')}}//plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">


    <link href="{{asset('assets/dashboard')}}//css/style.css" rel="stylesheet">
    <link href="{{asset('assets/dashboard')}}//css/colors/default.css" rel="stylesheet">
    <link href="{{asset('assets/dashboard')}}//css/colors/color2.css" rel="stylesheet">
</head>

<body class="main-body" cz-shortcut-listen="true">
<!-- Loader -->
<div id="global-loader">
    <svg version="1.1" id="L1" xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" viewBox="0 0 100 100" enable-background="new 0 0 100 100" xml:space="preserve">
           <circle fill="none" stroke="#fff" stroke-width="6" stroke-miterlimit="15" stroke-dasharray="14.2472,14.2472" cx="50" cy="50" r="47">
               <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="5s" from="0 50 50" to="360 50 50" repeatCount="indefinite"></animateTransform>
           </circle>
        <circle fill="none" stroke="#718be4" stroke-width="1" stroke-miterlimit="10" stroke-dasharray="10,10" cx="50" cy="50" r="39">
            <animateTransform attributeName="transform" attributeType="XML" type="rotate" dur="5s" from="0 50 50" to="-360 50 50" repeatCount="indefinite"></animateTransform>
        </circle>
        <g fill="#fff">
            <rect x="30" y="35" width="5" height="30">
                <animateTransform attributeName="transform" dur="1s" type="translate" values="0 5 ; 0 -5; 0 5" repeatCount="indefinite" begin="0.1"></animateTransform>
            </rect>
            <rect x="40" y="35" width="5" height="30">
                <animateTransform attributeName="transform" dur="1s" type="translate" values="0 5 ; 0 -5; 0 5" repeatCount="indefinite" begin="0.2"></animateTransform>
            </rect>
            <rect x="50" y="35" width="5" height="30">
                <animateTransform attributeName="transform" dur="1s" type="translate" values="0 5 ; 0 -5; 0 5" repeatCount="indefinite" begin="0.3"></animateTransform>
            </rect>
            <rect x="60" y="35" width="5" height="30">
                <animateTransform attributeName="transform" dur="1s" type="translate" values="0 5 ; 0 -5; 0 5" repeatCount="indefinite" begin="0.4"></animateTransform>
            </rect>
            <rect x="70" y="35" width="5" height="30">
                <animateTransform attributeName="transform" dur="1s" type="translate" values="0 5 ; 0 -5; 0 5" repeatCount="indefinite" begin="0.5"></animateTransform>
            </rect>
        </g>
       </svg>
</div>
<!-- End Loader -->
<!-- Page -->
<div class="page main-signin-wrapper construction">
    <!-- Row -->
    <div class="signpages">
        <div class="card">
            <div class="card-body">
                <img src="{{asset('assets/dashboard')}}//img/logo.svg" class="header-brand-img text-center mb-4" alt="logo">
                <form action="{{route('login')}}" method="post">
                    @csrf
                    <h5 class="title">تسجيل الدخول</h5>
                    <div class="form-group text-start"><label>البريد الالكتروني</label>
                        <input class="form-control" placeholder="" name="email" type="text">
                        @error('email')
                        <span class="text-danger">{{$message}}</span>
                        @enderror
                    </div>
                    <div class="form-group text-start"><label>كلمة المرور</label>
                        <input class="form-control" placeholder="" name="password" type="password">
                    </div>

                    <button type="submit" class="btn ripple btn-light btn-block font-btn">تسجيل الدخول</button>
                </form>

            </div>
        </div>
    </div>
</div>

<script src="{{asset('assets/dashboard')}}//plugins/jquery/jquery.min.js"></script>
<script src="{{asset('assets/dashboard')}}//plugins/bootstrap/js/popper.min.js"></script>
<script src="{{asset('assets/dashboard')}}//plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="{{asset('assets/dashboard')}}//js/custom.js"></script>


</body>

</html>
