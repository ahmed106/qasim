@extends('website.layouts.master')
@push('title')
    {{$page_name}}
@endpush
@section('content')
    <div class="ttm-page-title ttm-bg clearfix" style="background-image: url({{asset('assets/website')}}/images/contact.jpg);">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="ttm-page-title-row-inner">
                        <div class="page-title-heading">
                            <h2 class="title">{{$page_name}}</h2>
                        </div>
                        <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{url('/')}}">
                                        <i class="themifyicon ti-home"></i> &nbsp;
                                        الرئيسية</a>
                                </span>
                            <span>{{$page_name}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page-title end -->
    <!--site-main start-->
    <div class="site-main">
        <!--form-section-->
        <section class="ttm-row form-section ttm-bgcolor-grey clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!--section-title-->
                        <div class="section-title title-style-center_text margin_bottom30">
                            <div class="title-header">
                                <h3>{{$page_name}}</h3>
                                <h2 class="title">لديك استفسار اترك <span>لنا رسالة !</span></h2>
                            </div>
                        </div>
                        <!--section-title-end-->
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-8">
                        <div class=" ttm-bgcolor-white p-40 res-991-margin_right0 ">
                            <form id="contact_form" class="wrap-form contact_form padding_top15" method="post" action="{{route('website.contactUs')}}">
                                @csrf
                                <div class="row ttm-boxes-spacing-30px">
                                    <div class="col-sm-6 ttm-box-col-wrapper">
                                        <label>
                                            <span class="text-input margin_bottom0"><input name="name" type="text" value="" placeholder="الاسم*" required="required"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-6 ttm-box-col-wrapper">
                                        <label>
                                            <span class="text-input margin_bottom0"><input name="phone" type="text" value="" placeholder="رقم الهاتف*" required="required"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 ttm-box-col-wrapper">
                                        <label>
                                            <span class="text-input margin_bottom0"><input name="email" type="text" value="" placeholder="البريد الالكتروني*" required="required"></span>
                                        </label>
                                    </div>
                                    <div class="col-sm-12 ttm-box-col-wrapper">
                                        <label>
                                            <span class="text-input margin_bottom0"><textarea name="body" rows="3" placeholder="الرسالة" aria-required="true"></textarea></span>
                                        </label>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <button class="submit ttm-btn ttm-btn-size-md ttm-btn-shape-square ttm-btn-style-fill ttm-btn-color-skincolor w-100" type="submit">ارسال</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="ttm-bgcolor-white p-40 res-991-margin_top30 ttm-bgcolor-white p-40 ">
                            <div class="featured-icon-box icon-align-top-content margin_bottom20 ">
                                <div class="featured-icon">
                                    <div class="ttm-icon ttm-icon_element-onlytxt ttm-icon_element-color-skincolor ttm-icon_element-size-lg">
                                        <i class="flaticon flaticon-message"></i>
                                    </div>
                                </div>
                                <div class="featured-content pt-2">
                                    <div class="featured-title ">
                                        <h3 class="margin_bottom0">راسلنا على</h3>
                                    </div>
                                    <div class="featured-desc">
                                        <p>{{$setting->website_email}}</p>
                                    </div>
                                </div>
                            </div>
                            <div class="featured-icon-box icon-align-top-content margin_bottom20 ">
                                <div class="featured-icon">
                                    <div class="ttm-icon ttm-icon_element-onlytxt ttm-icon_element-color-skincolor ttm-icon_element-size-lg">
                                        <i class="flaticon flaticon-design-team"></i>
                                    </div>
                                </div>
                                <div class="featured-content pt-2">
                                    <div class="featured-title ">
                                        <h3 class="margin_bottom0">اتصل بنا على</h3>
                                    </div>
                                    <div class="featured-desc phone-num">
                                        <p>{{$setting->website_phone}}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--form-section-end-->
        <!--map-section-->
        <section class="ttm-row padding_top_zero-section mt_100 res-991-margin_top40 res-991-margin_bottom40 clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-6">
                        <div class="featured-icon-box icon-align-before-content style9">
                            <div class="featured-icon">
                                <div class="ttm-icon ttm-icon_element-onlytxt ttm-icon_element-color-skincolor ttm-icon_element-size-md">
                                    <i class="flaticon flaticon-location-1"></i>
                                </div>
                            </div>
                            <div class="featured-content">
                                <div class="featured-title">
                                    <h3>العنوان</h3>
                                </div>
                                <div class="featured-desc">
                                    <p>{{$setting->website_address}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-6">
                        <div class="featured-icon-box icon-align-before-content style9 active">
                            <div class="featured-icon">
                                <div class="ttm-icon ttm-icon_element-onlytxt ttm-icon_element-color-skincolor ttm-icon_element-size-md">
                                    <i class="flaticon flaticon-call-1"></i>
                                </div>
                            </div>
                            <div class="featured-content">
                                <div class="featured-title">
                                    <h3>الهاتف</h3>
                                </div>
                                <div class="featured-desc phone-num">
                                    <p>{{$setting->website_phone}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-12">
                        <div class="featured-icon-box icon-align-before-content style9">
                            <div class="featured-icon">
                                <div class="ttm-icon ttm-icon_element-onlytxt ttm-icon_element-color-skincolor ttm-icon_element-size-md">
                                    <i class="flaticon flaticon-envelope"></i>
                                </div>
                            </div>
                            <div class="featured-content">
                                <div class="featured-title">
                                    <h3>البريد الالكتروني</h3>
                                </div>
                                <div class="featured-desc">
                                    <p>{{$setting->website_email}}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--map-section-end-->
        <div id="google_map" class="google_map mt_90 res-991-margin_top0">
            <div class="map_container clearfix">
                <div id="map">
                    {!! $setting?$setting->map:'' !!}
                </div>
            </div>
        </div>
    </div>
@endsection


