@extends('website.layouts.master')
@push('title')
    {{$page_name}} |  {{$blog->title}}
@endpush
@section('content')

    <div class="ttm-page-title-row ttm-bg ttm-bgimage-yes ttm-bgcolor-dark clearfix">
        <div class="ttm-row-wrapper-bg-layer ttm-bg-layer"></div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="ttm-page-title-row-inner">
                        <div class="page-title-heading">
                            <h2 class="title">{{$page_name}}</h2>
                        </div>
                        <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{url('')}}">
                                        <i class="themifyicon ti-home"></i> &nbsp;
                                        الرئيسية</a>
                                </span>
                            <span>{{$page_name}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page-title end -->
    <!--site-main start-->
    <div class="site-main">
        <!--blog-classic-section -->
        <div class="ttm-row sidebar ttm-sidebar-right clearfix">
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-lg-8 content-area">
                        <!-- post -->
                        <article class="post ttm-blog-single clearfix">
                            <!-- post-featured-wrapper -->
                            <div class="ttm-post-featured-wrapper ttm-featured-wrapper">
                                <div class="ttm-post-featured">
                                    <img class="img-fluid" src="{{$blog->image}}" alt="">
                                </div>
                            </div><!-- post-featured-wrapper end -->
                            <!-- ttm-blog-classic-content -->
                            <div class="ttm-blog-single-content">
                                <div class="ttm-post-entry-header">
                                    <div class="post-meta">
                                        <span class="ttm-meta-line byline"><i class="ti ti-user"></i> {{$blog->creator->name}} </span>
                                        <span class="ttm-meta-line tags-links"><i class="fa fa-calendar"></i>{{\Carbon\Carbon::create($blog->created_at)->monthName}} {{\Carbon\Carbon::create($blog->created_at)->day}}, {{\Carbon\Carbon::create($blog->created_at)->year}}</span>
                                    </div>
                                </div>
                                <div class="entry-content">
                                    <h3>{{$blog->title}}</h3>
                                    <div class="ttm-box-desc-text">
                                        <p>
                                            {!! $blog->content !!}
                                        </p>
                                    </div>

                                    @if($blog->file && $blog->file !=null)
                                        <div>

                                            <a target="_blank" href="{{asset('images/files/'.$blog->file)}}" class="btn btn-sm  btn-info text-white">

                                                <i class="fa fa-eye"></i>
                                                عرض الملف
                                            </a>
                                        </div>
                                    @endif


                                    <div class="ttm-horizontal_sep width-100 margin_top30 margin_bottom30"></div>
                                    <div class="row ">
                                        @if($prevBlog)
                                            <div class="col-sm-6">
                                                <div class="featured-icon-box icon-align-before-content icon-ver_align-top">
                                                    <div class="featured-icon">
                                                        <img width="130" height="100" class="img-fluid auto_size" src="{{$prevBlog->image}}" alt="ttm_single_image-wrapper">
                                                    </div>

                                                    <div class="featured-content padding_left15 padding_top5">
                                                        <div class="featured-desc">
                                                            <a href="{{url('blogs/'.$prevBlog->id)}}"><p>المقال السابق</p></a>
                                                        </div>
                                                        <div class="featured-title">
                                                            <h4><a href="{{url('blogs/'.$prevBlog->id)}}">{{$prevBlog->title}}</a></h4>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                        @if($nextBlog)
                                            <div class="col-sm-6">
                                                <div class="featured-icon-box icon-align-before-content icon-ver_align-top ">
                                                    <div class="featured-content text-end padding_right15 padding_top5 ">
                                                        <div class="featured-desc">
                                                            <a href="{{url('blogs/'.$nextBlog->id)}}"><p>المقال التالي</p></a>
                                                        </div>
                                                        <div class="featured-title">
                                                            <h4><a href="{{url('blogs/'.$nextBlog->id)}}">{{$nextBlog->title}}</a></h4>
                                                        </div>
                                                    </div>
                                                    <div class="featured-icon">
                                                        <img width="150" height="150" class="img-fluid auto_size" src="{{$nextBlog->image}}" alt="ttm_single_image-wrapper">
                                                    </div>
                                                </div>
                                            </div>
                                        @endif

                                    </div>

                                </div>
                            </div><!-- ttm-blog-classic-content end -->
                        </article><!-- post end -->
                    </div>
                    <div class="col-lg-4 widget-area sidebar-right">
                        <aside class="widget widget-recent-post with-title">
                            <h3 class="widget-title">مقالات اخري</h3>
                            <ul class="widget-post ttm-recent-post-list">
                                @foreach($blogs as $blog)
                                    <li>
                                        <a href="{{url('blogs/'.$blog->id)}}"><img class="img-fluid" src="{{$blog->image}}" alt="post-img"></a>
                                        <div class="post-detail">
                                            <span class="post-date"><i class="fa fa-calendar"></i>{{\Carbon\Carbon::create($blog->created_at)->monthName}} {{\Carbon\Carbon::create($blog->created_at)->day}}, {{\Carbon\Carbon::create($blog->created_at)->year}}</span>
                                            <a href="{{url('blogs/'.$blog->id)}}">{{$blog->title}}</a>
                                        </div>
                                    </li>
                                @endforeach


                            </ul>
                        </aside>
                    </div>
                </div><!-- row end -->
            </div>
        </div>
        <!--blog-classic-section-end-->
    </div>
@endsection
