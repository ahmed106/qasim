@extends('website.layouts.master')

@push('title')
{{$page_name}}
@endpush
@section('content')
    <div class="ttm-page-title-row ttm-bg ttm-bgimage-yes ttm-bgcolor-dark clearfix">
        <div class="ttm-row-wrapper-bg-layer ttm-bg-layer"></div>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="ttm-page-title-row-inner">
                        <div class="page-title-heading">
                            <h2 class="title">{{$page_name}}</h2>
                        </div>
                        <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{url('')}}">
                                        <i class="themifyicon ti-home"></i>
                                        الرئيسية</a>
                                </span>
                            <span>{{$page_name}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page-title end -->
    <!--site-main start-->
    <div class="site-main">
        <!-- blog-grid-section -->
        <section class="ttm-row blog-grid-view clearfix">
            <div class="container">
                <div class="row">
                    @foreach($blogs as $blog)
                        <div class="col-lg-4 col-md-4 col-sm-12">
                            <!-- featured-imagebox-post -->
                            <div class="featured-imagebox featured-imagebox-post style1">
                                <a href="{{url('blogs/'.$blog->id)}}" class="featured-thumbnail">
                                    <img class="img-fluid" src="{{$blog->image}}" alt="">
                                </a>
                                <div class="featured-content">
                                    <!-- ttm-box-post-date -->
                                    <div class="ttm-box-post-date">
                                        <span class="ttm-entry-date">
                                            <time class="entry-date" datetime="2021-05-18T04:16:25+00:00">{{\Carbon\Carbon::create($blog->created_at)->day}}<span class="entry-month entry-year">{{\Carbon\Carbon::create($blog->created_at)->monthName}}</span></time>
                                        </span>
                                    </div><!-- ttm-box-post-date end -->
                                    <div class="post-meta">
                                        <span class="ttm-meta-line byline">
                                            <i class="ti ti-user"></i> {{$blog->creator->name}} </span>
                                    </div>
                                    <div class="featured-title">
                                        <h3><a href="{{url('blogs/'.$blog->id)}}">{{$blog->title}}</a></h3>
                                    </div>
                                    <div class="featured-desc">
                                        {!! strip_tags(substr($blog->content,0,200)) !!}
                                    </div>
                                    <a class="ttm-btn btn-inline ttm-btn-size-md ttm-icon-btn-right ttm-textcolor-darkgreycolor" href="{{url('blogs/'.$blog->id)}}">قراءة المزيد<i class="fa fa-long-arrow-left"></i></a>
                                </div>
                            </div><!-- featured-imagebox-post end -->
                        </div>
                    @endforeach


                </div>
            </div>
        </section>
        <!--blog-grid-section-end-->
        <!--client-section-->
        <section class="ttm-row padding_zero-section clearfix">
            <div class="container">
                <div class="row">
                    <h3 class="title">عملائنا الكرام </h3>
                </div>
                <div class="row slick_slider" data-slick='{"slidesToShow": 5, "slidesToScroll": 1, "arrows":false, "autoplay":false, "infinite":true, "responsive": [{"breakpoint":1200,"settings":{"slidesToShow": 3}}, {"breakpoint":1024,"settings":{"slidesToShow": 3}}, {"breakpoint":777,"settings":{"slidesToShow": 1}},{"breakpoint":575,"settings":{"slidesToShow": 1}},{"breakpoint":400,"settings":{"slidesToShow": 1}}]}'>
                    @foreach($customers as $customer)
                        <div class="col-lg-12">
                            <div class="client-box">
                                <div class="client-thumbnail res-575-margin_top0">
                                    <img class="img-fluid auto_size" width="160" height="98" src="{{$customer->image}}" alt="image">
                                </div>
                            </div>
                        </div>
                    @endforeach


                </div><!-- row end -->
            </div>
        </section>
        <!--client-section-end-->
    </div>
@endsection
