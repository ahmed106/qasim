@extends('website.layouts.master')
@push('title')
    {{$page_name}}
@endpush
@section('content')
    <div class="ttm-page-title ttm-bg clearfix" style="background-image: url({{asset('assets/website/images/aboutus.jpg')}});">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="ttm-page-title-row-inner">
                        <div class="page-title-heading">
                            <h2 class="title">{{$page_name}}
                            </h2>
                        </div>
                        <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{url('/')}}">
                                        <i class="themifyicon ti-home"></i> &nbsp;
                                        الرئيسية</a>
                                </span>
                            <span>{{$page_name}}
                                </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page-title end -->
    <!--site-main start-->
    <div class="site-main">
        <!-- about-section -->
        <section class="ttm-row about-section clearfix ">
            <div class="container">
                <div class="row g-0 res-991-padding_lr15 ">
                    <div class="col-lg-6">
                        <div class="section-title">
                            <div class="title-header">
                                <h3>عن الشركة</h3>
                                <h2 class="title">{{$about?$about->title:''}} </h2>
                            </div>
                            <div class="title-desc">
                                {!! $about?$about->content :''!!}
                            </div>
                        </div>
                        <div class="ttm-tabs ttm-tab-style-04 clearfix margin_top30" data-effect="fadeIn">
                            <ul class="tabs">
                                <li class="tab active"><a href="#">مهمتنا</a></li>
                                <li class="tab"><a href="#">رؤيتنا</a></li>
                                <li class="tab"><a href="#">طموحاتنا</a></li>
                            </ul>
                            <div class="content-tab padding_top30 padding_bottom30">
                                <!-- content-inner -->
                                <div class="content-inner active">
                                    <div class="ttm-tabs-desc">
                                        <p>{!! $about?$about->missions:'' !!}</p>
                                    </div>
                                    {{--                                    <div class="row g-0">--}}
                                    {{--                                        <div class="col-sm-4">--}}
                                    {{--                                            <div class="tab-figure res-767-mb-20">--}}
                                    {{--                                                <img class="img-fluid auto_size" src="{{asset('assets/website')}}/images/tab1-300x200.jpg" alt="image" height="80" width="120">--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <div class="col-sm-8">--}}
                                    {{--                                            <ul class="ttm-list ttm-list-style-icon d-grid ttm-list-icon-color-skincolor margin_top10 ml_30 res-575-margin_left0">--}}
                                    {{--                                                <li><i class="fa fa-angle-double-left"></i><span class="ttm-list-li-content">سوق للمشاريع الصناعية أو الخاصة</span></li>--}}
                                    {{--                                                <li><i class="fa fa-angle-double-left"></i><span class="ttm-list-li-content">خدمة كاملة مجربة وموثوقة وموثوق بها</span></li>--}}
                                    {{--                                                <li><i class="fa fa-angle-double-left"></i><span class="ttm-list-li-content">الخبرة من الخبرة الواسعة</span></li>--}}
                                    {{--                                            </ul>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                </div><!-- content-inner end-->
                                <!-- content-inner -->
                                <div class="content-inner">
                                    <div class="ttm-tabs-desc">
                                        <p>{!! $about?$about->vision:'' !!}</p>
                                    </div>
                                    {{--                                    <div class="row">--}}
                                    {{--                                        <div class="col-sm-4">--}}
                                    {{--                                            <div class="tab-figure  g-0 res-767-mb-20">--}}
                                    {{--                                                <img class="img-fluid auto_size" src="{{asset('assets/website')}}/images/tab2-300x200.jpg" alt="image" height="80" width="120">--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <div class="col-sm-8">--}}
                                    {{--                                            <ul class="ttm-list ttm-list-style-icon d-grid ttm-list-icon-color-skincolor margin_top10 ml_30 res-575-margin_left0">--}}
                                    {{--                                                <li><i class="fa fa-angle-double-left"></i><span class="ttm-list-li-content">سوق للمشاريع الصناعية أو الخاصة</span></li>--}}
                                    {{--                                                <li><i class="fa fa-angle-double-left"></i><span class="ttm-list-li-content">خدمة كاملة مجربة وموثوقة وموثوق بها</span></li>--}}
                                    {{--                                                <li><i class="fa fa-angle-double-left"></i><span class="ttm-list-li-content">الخبرة من الخبرة الواسعة</span></li>--}}
                                    {{--                                            </ul>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                </div><!-- content-inner end-->
                                <!-- content-inner -->
                                <div class="content-inner">
                                    <div class="ttm-tabs-desc">
                                        <p>{!! $about?$about->plans:'' !!}</p>
                                    </div>
                                    {{--                                    <div class="row g-0">--}}
                                    {{--                                        <div class="col-sm-4">--}}
                                    {{--                                            <div class="tab-figure res-767-mb-20">--}}
                                    {{--                                                <img class="img-fluid auto_size" src="{{asset('assets/website')}}/images/tab3-300x200.jpg" alt="image" height="80" width="120">--}}
                                    {{--                                            </div>--}}
                                    {{--                                        </div>--}}
                                    {{--                                        <div class="col-sm-8">--}}
                                    {{--                                            <ul class="ttm-list ttm-list-style-icon d-grid ttm-list-icon-color-skincolor margin_top10 ml_30 res-575-margin_left0">--}}
                                    {{--                                                <li><i class="fa fa-angle-double-left"></i><span class="ttm-list-li-content">سوق للمشاريع الصناعية أو الخاصة</span></li>--}}
                                    {{--                                                <li><i class="fa fa-angle-double-left"></i><span class="ttm-list-li-content">خدمة كاملة مجربة وموثوقة وموثوق بها</span></li>--}}
                                    {{--                                                <li><i class="fa fa-angle-double-left"></i><span class="ttm-list-li-content">الخبرة من الخبرة الواسعة</span></li>--}}
                                    {{--                                            </ul>--}}
                                    {{--                                        </div>--}}
                                    {{--                                    </div>--}}
                                </div><!-- content-inner end-->
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="ttm_single_image-wrapper text-end res-991-padding_top30">
                            @if($about)
                                <img class="img-fluid auto_size" src="{{$about->image}}" alt="single-06" height="597" width="569">

                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--about-section-end-->
        <!--fid-section-->
        <section class="ttm-row padding_top_zero-section clearfix">
            <div class="container">
                <div class="row g-0">
                    <div class="col-lg-12">
                        <div class="row g-0">
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="ttm-fid inside ttm-fid-with-icon style6">
                                    <div class="ttm-fid-icon-wrapper ttm-icon_element-color-skincolor">
                                        <i class="flaticon flaticon-sketch"></i>
                                    </div>
                                    <div class="ttm-fid-contents">
                                        <h4 class="ttm-fid-inner">

                                            <span data-appear-animation="animateDigits" data-from="0" data-to="{{$about?$about->complete_projects_number:''}}" data-interval="5" data-before="" data-before-style="sup" data-after="+" data-after-style="sub" class="numinate">{{$about?$about->complete_projects_number:''}}</span>
                                        </h4>
                                        <h3 class="ttm-fid-title">مشروع مكتمل</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="ttm-fid inside ttm-fid-with-icon style6">
                                    <div class="ttm-fid-icon-wrapper ttm-icon_element-color-skincolor">
                                        <i class="flaticon flaticon-house"></i>
                                    </div>
                                    <div class="ttm-fid-contents">
                                        <h4 class="ttm-fid-inner">
                                            <span data-appear-animation="animateDigits" data-from="0" data-to="{{$about?$about->prizes_number:''}}" data-interval="1" data-before="" data-before-style="sup" data-after="+" data-after-style="sub" class="numinate">{{$about?$about->prizes_number:''}}</span>
                                        </h4>
                                        <h3 class="ttm-fid-title">الجوائز</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="ttm-fid inside ttm-fid-with-icon style6">
                                    <div class="ttm-fid-icon-wrapper ttm-icon_element-color-skincolor">
                                        <i class="flaticon flaticon-design-team"></i>
                                    </div>
                                    <div class="ttm-fid-contents">
                                        <h4 class="ttm-fid-inner">
                                            <span data-appear-animation="animateDigits" data-from="0" data-to="{{$about?$about->customers_number:''}}" data-interval="5" data-before="" data-before-style="sup" data-after="+" data-after-style="sub" class="numinate">{{$about?$about->customers_number:''}}</span>
                                        </h4>
                                        <h3 class="ttm-fid-title">العملاء</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-6">
                                <div class="ttm-fid inside ttm-fid-with-icon style6">
                                    <div class="ttm-fid-icon-wrapper ttm-icon_element-color-skincolor">
                                        <i class="flaticon flaticon-customer-service"></i>
                                    </div>
                                    <div class="ttm-fid-contents">
                                        <h4 class="ttm-fid-inner">
                                            <span data-appear-animation="animateDigits" data-from="0" data-to="{{$about?$about->employee_number:''}}" data-interval="5" data-before="" data-before-style="sup" data-after="+" data-after-style="sub" class="numinate">{{$about?$about->employee_number:''}}</span>
                                        </h4>
                                        <h3 class="ttm-fid-title">الموظفين</h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--fid-section-end-->
        <!--client-section-->
        <div class="ttm-row padding_zero-section clearfix">
            <div class="container">
                <div class="row">
                    <h3 class="title">عملائنا الكرام </h3>
                </div>
                <div class="row slick_slider" data-slick='{"slidesToShow": 5, "slidesToScroll": 1, "arrows":false, "autoplay":false, "infinite":true, "responsive": [{"breakpoint":1200,"settings":{"slidesToShow": 3}}, {"breakpoint":1024,"settings":{"slidesToShow": 3}}, {"breakpoint":777,"settings":{"slidesToShow": 1}},{"breakpoint":575,"settings":{"slidesToShow": 1}},{"breakpoint":400,"settings":{"slidesToShow": 1}}]}'>
                    @if($customers->count()>0)
                        @foreach($customers as $customer)
                            <div class="col-lg-12">
                                <div class="client-box">
                                    <div class="client-thumbnail res-575-margin_top0">
                                        <img class="img-fluid auto_size" width="160" height="98" src="{{$customer->image}}" alt="image">
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif


                </div><!-- row end -->
            </div>
        </div>
        <!--client-section-end-->
    </div>
@endsection
