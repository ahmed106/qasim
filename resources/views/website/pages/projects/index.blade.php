@extends('website.layouts.master')
@push('title')
    {{$page_name}}
@endpush
@section('content')

    <div class="ttm-page-title ttm-bg clearfix" style="background-image: url({{asset('assets/website')}}/images/aboutus.jpg);">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="ttm-page-title-row-inner">
                        <div class="page-title-heading">
                            <h2 class="title">{{$page_name}}</h2>
                        </div>
                        <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{url('/')}}">
                                        <i class="themifyicon ti-home"></i> &nbsp;
                                        الرئيسية</a>
                                </span>
                            <span>{{$page_name}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page-title end -->
    <!--site-main start-->
    <div class="site-main">
        <!--grid-section-->
        <div class="ttm-row grid-section clearfix">
            <div class="container">
                <!-- row -->
                <div class="row ">
                    @foreach($projects as $project)
                        <div class="col-lg-4 col-md-6 col-sm-6">
                            <!-- featured-imagebox-portfolio -->
                            <div class="featured-imagebox featured-imagebox-portfolio style1">
                                <!-- ttm-box-view-overlay -->
                                <div class="ttm-box-view-overlay ttm-portfolio-box-view-overlay">
                                    <!-- featured-thumbnail -->
                                    <div class="featured-thumbnail">
                                        <a href="{{url('projects/'.$project->id)}}"> <img class="img-fluid" src="{{asset('images/projects/'.$project->photos->first()->src)}}" alt="image"></a>
                                    </div><!-- featured-thumbnail end-->
                                    <div class="ttm-media-link">
                                        <a href="{{url('projects/'.$project->id)}}" class="ttm_link"><i class="ti ti-plus"></i></a>
                                    </div>
                                </div><!-- ttm-box-view-overlay end-->
                                <div class="featured-content featured-content-portfolio">
                                    <div class="featured-title">
                                        <h3><a href="{{url('projects/'.$project->id)}}">{{$project->title}}</a></h3>
                                    </div>
                                </div>
                            </div><!-- featured-imagebox-portfolio -->
                        </div>
                    @endforeach


                </div><!-- row end -->
            </div>
        </div>
        <!--grid-section end-->
        <!--financial-strategies-section -->
        <section class="ttm-row financial-section ttm-bgcolor-grey clearfix">
            <div class="container">
                <div class="col-lg-12">
                    <div class="text-center">
                        <h3>نحن نقدم استراتيجيات مالية وخدمات متفوقة</h3>
                        <div class=" margin_top30">
                            <a class="ttm-btn ttm-btn-shape-square ttm-btn-style-fill ttm-btn-color-skincolor padding_top15 padding_bottom15 " href="{{url('contact-us')}}">احصل على الاسعار<i class="fa fa-long-arrow-left"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--financial-strategies-section-End -->
    </div>
@endsection
