@extends('website.layouts.master')
@push('title')
    {{$page_name}}
@endpush
@section('content')
    <div class="ttm-page-title ttm-bg clearfix" style="background-image: url({{asset('assets/website')}}/images/service-1.jpg);">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <div class="ttm-page-title-row-inner">
                        <div class="page-title-heading">
                            <h2 class="title">{{$page_name}}</h2>
                        </div>
                        <div class="breadcrumb-wrapper">
                                <span>
                                    <a title="Homepage" href="{{url('/')}}">
                                        <i class="themifyicon ti-home"></i> &nbsp;
                                        الرئيسية</a>
                                </span>
                            <span>{{$page_name}}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- page-title end -->
    <!--site-main start-->
    <div class="site-main">
        <!--service-section-->
        <section class="ttm-row padding_top_zero-section clearfix">
            <div class="container-fluid">
                <div class="row ttm-bgcolor-grey padding_top90 padding_bottom180">
                    <div class="col-lg-12">
                        <!--section-title -->
                        <div class="section-title title-style-center_text ">
                            <div class="title-header">
                                <h3>عن خدماتنا</h3>
                                <h2 class="title">خدمات نقدمها لك</h2>
                            </div>
                        </div>
                        <!--section-title-end -->
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row mt_160 res-1199-mt_150">
                    <div class="col-lg-12">
                        <div class="row">
                            @foreach($services as $service)
                                <div class="col-lg-4">
                                    <!-- featured-imagebox-post -->
                                    <div class="featured-imagebox featured-imagebox-sevices style1">
                                        <div class="featured-thumbnail">
                                            <a href="{{url('/services/'.$service->id)}}" tabindex="-1">
                                                <img class="img-fluid" src="{{$service->image}}"
                                                     alt="">
                                            </a>
                                        </div>
                                        <div class="featured-content">
                                            <div class="featured-title">
                                                <h3><a href="{{url('/services/'.$service->id)}}" tabindex="-1">{{$service->title}}</a></h3>
                                            </div>
                                            <div class="featured-desc ">

                                                {!! $service->content !!}
                                            </div>
                                        </div>
                                    </div>
                                    <!-- featured-imagebox-post end -->
                                </div>
                            @endforeach


                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--service-section-end-->
    </div>
@endsection
