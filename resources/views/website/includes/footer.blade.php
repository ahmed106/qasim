<footer class="footer widget-footer clearfix">
    <div class="first-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-4 widget-area">
                </div>


            </div>
        </div>
    </div>
    <div class="second-footer ttm-bgimage-yes bg-footer ttm-bg ttm-bgcolor-darkgrey">
        <div class="ttm-row-wrapper-bg-layer ttm-bg-layer"></div>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 widget-area">
                    <div class="widget-latest-tweets">
                        <div class="widgte-text">

                            <div class="widgte-title">
                                <h4>{{\App\Models\Page::where('url','/about')->first()->name}}</h4>
                            </div>
                            <div class="">
                                <p>
                                    {!! $about?$about->content:'' !!}
                                </p>
                            </div>
                            <div class="widget_social padding_top10 clearfix">
                                <div class="social-icons">
                                    <ul class="social-icons list-inline">
                                        <li><a class="tooltip-top" href="{{$setting?$setting->website_facebook:''}}" rel="noopener" aria-label="facebook"
                                               data-tooltip="Facebook">
                                                <i class="fa fa-facebook"></i></a></li>

                                        <li><a class="tooltip-top" href="{{$setting?$setting->website_linked_in:''}}" rel="noopener" aria-label="linkedin"
                                               data-tooltip="linkedin">
                                                <i class="fa fa-linkedin"></i></a></li>

                                        <li><a class="tooltip-top" href="{{$setting?$setting->website_twitter:''}}" rel="noopener"
                                               aria-label="twitter" data-tooltip="twitter"><i
                                                    class="fa fa-twitter"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 widget-area">
                    <div class="widget widget_nav_menu clearfix">
                        <h3 class="widget-title">روابط سريعة</h3>
                        <ul id="menu-footer-quick-links">
                            @foreach($pages->take(6) as $page)
                                <li><a href="{{$page->url}}"> <i class="fa fa-arrow-left"></i>{{$page->name}}</a></li>

                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 widget-area">
                    <div class="widget widget-recent-post clearfix">
                        <h3 class="widget-title">آخر المقالات</h3>
                        <ul class="widget-post ttm-recent-post-list">
                            @if($blogs->count()>0)
                                @foreach($blogs->last()->get()->take(2) as $blog)
                                    <li>
                                        <a href="{{url('blogs/'.$blog->id)}}"><img class="img-fluid"
                                                                                   src="{{$blog->image}}" alt="post-img"></a>
                                        <div class="post-detail">
                                            <span class="post-date"><i class="fa fa-calendar"></i>{{\Carbon\Carbon::create($blog->created_at)->monthName}} {{\Carbon\Carbon::create($blog->created_at)->day}}, {{\Carbon\Carbon::create($blog->created_at)->year}}</span>
                                            <a href="{{url('blogs/'.$blog->id)}}">
                                                {!! $blog->title !!}
                                            </a>
                                        </div>
                                    </li>
                                @endforeach
                            @endif


                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="bottom-footer-text ttm-bg copyright">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="text-left">
                            <span class="cpy-text"> © 2021 <a href="https://www.codlop.sa">Codlop</a> جميع الحقوق محفوظة. </span>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <ul class="footer-nav-menu">

                            @foreach($pages->take(3) as $page)
                                <li><a href="{{url($page->url)}}">{{$page->name}}</a></li>

                            @endforeach

                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<a id="totop" href="#top">
    <i class="fa fa-angle-up"></i>
</a>
<!--back-to-top end-->
