<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="keywords" content="HTML5 Template"/>
    <meta name="description" content="Template-2"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Security-Policy" content="upgrade-insecure-requests">
    {!! $setting?$setting->header:'' !!}
    <title>{{$setting->website_name}} | @stack('title')</title>
    @include('website.includes.css')
    @stack('css')

    <style>
        .float {
            position: fixed;
            width: 60px;
            height: 60px;
            bottom: 40px;
            right: 40px;
            background-color: #25d366;
            color: #FFF !important;
            border-radius: 50px;
            text-align: center;
            font-size: 30px;
            z-index: 100;
        }

        .my-float {
            margin-top: 16px;
        }
    </style>
</head>

<body>
<!--page start-->
<div class="page">
    <!-- preloader start -->
    <div id="preloader">
        <div id="status"></div>
    </div>
    <!-- preloader end -->
    @include('website.includes.header')
    @yield('content')

    @include('website.includes.footer')


</div><!-- page end -->


<a href="https://wa.me/{{$setting->website_phone}}" class="float" target="_blank">
    <i class="fa fa-whatsapp my-float"></i>
</a>
@include('website.includes.js')

@stack('js')
{!! $setting?$setting->footer:'' !!}
</body>

</html>
