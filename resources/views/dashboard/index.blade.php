@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">
                <div class="row row-sm">
                    <div class="col-12 col-sm-6 col-md-6 col-xl-3">
                        <a href="{{route('blogs.index')}}" class="card custom-card">
                            <div class="card-body">
                                <div class="card-item">
                                    <div class="card-item-icon card-icon">
                                        <img src="{{asset('assets/dashboard')}}/img/analysis.png" alt="">
                                    </div>
                                    <div class="card-item-title mb-2"><label
                                            class="main-content-label tx-13 font-weight-bold mb-1">عدد المقالات</label>

                                    </div>
                                    <div class="card-item-body">
                                        <div class="card-item-stat">
                                            <h4 class="font-weight-bold">{{$data['blogs_count']}}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-xl-3">
                        <a href="{{route('customers.index')}}" class="card custom-card">
                            <div class="card-body">
                                <div class="card-item">
                                    <div class="card-item-icon card-icon">
                                        <img src="{{asset('assets/dashboard')}}/img/analysis.png" alt="">
                                    </div>
                                    <div class="card-item-title mb-2"><label
                                            class="main-content-label tx-13 font-weight-bold mb-1">عدد العملاء</label>

                                    </div>
                                    <div class="card-item-body">
                                        <div class="card-item-stat">
                                            <h4 class="font-weight-bold">{{$data['customers_count']}}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-xl-3">
                        <a href="{{route('services.index')}}" class="card custom-card">
                            <div class="card-body">
                                <div class="card-item">
                                    <div class="card-item-icon card-icon">
                                        <img src="{{asset('assets/dashboard')}}/img/analysis.png" alt="">
                                    </div>
                                    <div class="card-item-title  mb-2"><label
                                            class="main-content-label tx-13 font-weight-bold mb-1">عدد الخدمات</label>

                                    </div>
                                    <div class="card-item-body">
                                        <div class="card-item-stat">
                                            <h4 class="font-weight-bold">{{$data['services_count']}}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-12 col-sm-6 col-md-6 col-xl-3">
                        <a href="{{route('projects.index')}}" class="card custom-card">
                            <div class="card-body">
                                <div class="card-item">
                                    <div class="card-item-icon card-icon">
                                        <img src="{{asset('assets/dashboard')}}/img/analysis.png" alt="">
                                    </div>
                                    <div class="card-item-title  mb-2"><label
                                            class="main-content-label tx-13 font-weight-bold mb-1">عدد المشاريع</label>

                                    </div>
                                    <div class="card-item-body">
                                        <div class="card-item-stat">
                                            <h4 class="font-weight-bold">{{$data['projects_count']}}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h3>العملاء</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">


                                    <div class="col-md-12">
                                        <div id="chart4" class="chart"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-header">
                                <h3>المقالات</h3>
                            </div>
                            <div class="card-body">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div id="chart3" class="chart"></div>
                                    </div>

                                </div>
                            </div>
                        </div>

                    </div>
                </div>


            </div>
        </div>
    </div> <!-- End Main Content-->

@endsection
@push('js')
    <script>
        $(function () {
            $('.datepicker').daterangepicker({
                locale: {
                    format: 'DD/MM/YYYY'
                },
                singleDatePicker: true,
                showDropdowns: true,
                autoApply: true,
                opens: 'left',
                minYear: 2010
            });
        });

    </script>
    <script>
        $(document).ready(function () {

            table.buttons().container()
                .appendTo('#DataTables_Table_0_wrapper .col-md-6:eq(0)');
        });

    </script>
    <script>
        $(document).ready(function () {
            $('.select2').select2({
                searchInputPlaceholder: 'ابحث هنا',
                width: '100%'
            });
            $('.select2-modal').select2({
                searchInputPlaceholder: 'ابحث هنا',
                width: '100%',
                dropdownParent: $(".modal")
            });
        });

    </script>
    <script>
        $('.input-images').imageUploader();

    </script>
    <script>
        $(document).ready(function () {
            $('.magnific-img').magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                closeBtnInside: false,
                fixedContentPos: true,
                mainClass: 'mfp-no-margins mfp-with-zoom',
                image: {
                    verticalFit: true
                },
                zoom: {
                    enabled: true,
                    duration: 500
                }
            });
        });

    </script>
    <script>
        var myChart = echarts.init(document.getElementById('chart3'));
        option = {
            tooltip: {
                trigger: 'axis',
                axisPointer: {
                    type: 'shadow'
                },
                textStyle: {
                    fontFamily: 'Bahij_Plain'
                }
            },
            legend: {
                // bottom: 0,
                textStyle: {
                    fontSize: 16,
                    fontFamily: 'Bahij_Plain'
                }
            },
            grid: {
                left: '3%',
                right: '4%',
                bottom: '3%',
                containLabel: true
            },
            xAxis: [{
                type: 'category',
                data: {!! $monthsList !!},
                axisLabel: {
                    textStyle: {
                        fontSize: 15,
                        fontFamily: 'Bahij_Plain'
                    }
                }
            }],
            yAxis: [{
                type: 'value'
            }],
            series: [{
                name: 'نشط',
                type: 'bar',
                stack: 'Ad',
                data: {!! $blogsList !!},
                color: '#00BEEC'
            }
            ]
        };

        myChart.setOption(option);

    </script>
    <script>
        var myChart = echarts.init(document.getElementById('chart4'));
        var colorPalette = ['#FFAE00', '#00BEEC'];
        option = {
            tooltip: {
                trigger: 'item',
                textStyle: {
                    fontFamily: 'Bahij_Plain'
                }
            },
            series: [{
                type: 'pie',
                radius: ['40%', '70%'],
                avoidLabelOverlap: false,
                itemStyle: {
                    borderRadius: 10,
                    borderColor: '#fff',
                    borderWidth: 2
                },
                label: {
                    textStyle: {
                        fontFamily: 'Bahij_Plain',
                        fontSize: '20'
                    }
                },
                emphasis: {
                    label: {
                        fontWeight: 'bold'
                    }
                },
                labelLine: {
                    show: true
                },
                data: [{
                    value: {!! $adminsList !!},
                    name: 'نشط'
                }
                ],
                color: colorPalette
            }],
            graph: {
                color: colorPalette
            }
        };
        myChart.setOption(option);

    </script>
@endpush
