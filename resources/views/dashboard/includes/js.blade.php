<!-- Back-to-top -->
<a href="#top" id="back-to-top"><i class="fe fe-arrow-up"></i></a>

<script src="{{asset('assets/dashboard')}}/plugins/jquery/jquery.min.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/bootstrap/js/popper.min.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/bootstrap/js/bootstrap.min.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/peity/jquery.peity.min.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/select2/js/select2.min.js"></script>
<script src="{{asset('assets/dashboard')}}/js/select2.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/sidemenu/sidemenu.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/sidebar/sidebar.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/uploader/image-uploader.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/daterangepicker/moment.min.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/daterangepicker/daterangepicker.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/magnific-popup/jquery.magnific-popup.js"></script>
<script src="{{asset('assets/dashboard')}}/js/echarts.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/steps/jquery-steps.min.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/dataTable/jquery.dataTables.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/dataTable/dataTables.bootstrap5.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/dataTable/Buttons/js/dataTables.buttons.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/dataTable/Buttons/js/buttons.bootstrap5.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/dataTable/JSZip/jszip.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/dataTable/pdfmake/pdfmake.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/dataTable/pdfmake/vfs_fonts.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/dataTable/Buttons/js/buttons.html5.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/dataTable/Buttons/js/buttons.print.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/jstree/jstree.js"></script>
<script src="{{asset('assets/dashboard')}}/plugins/ckeditor/ckeditor.js"></script>
<script src="{{asset('assets/dashboard')}}/js/sticky.js"></script>
<script src="{{asset('assets/dashboard')}}/js/custom.js"></script>
<script src="{{asset('assets/dashboard/plugins/toastr/toastr.js')}}"></script>
<script src="{{asset('assets/dashboard/plugins/sweet_alert/swal.js')}}"></script>

@toastr_render
@include('dashboard.includes.js_helper')
@include('dashboard.includes.swal')
<script>
    $("textarea.editor").each(function () {
        var txt = $(this).attr('name');
        CKEDITOR.replace(txt, {
            language: 'ar'
        });
    });
</script>

@stack('js')

