<script>
    $(function () {
        // image preview
        $('#photo').on('change', function (e) {
            let file = e.target.files[0];
            let url = URL.createObjectURL(file)
            $(this).parents('.row').find('#preview').attr('src', url)
        }); //end image preview

        // delete btn
        $(document).on('click', '#delete_btn', function (e) {
            e.preventDefault();
            let form = $(this).parent().find('#delete_form');
            Swal.fire({
                title: 'هل أنت متأكد',
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم',
                cancelButtonText: 'لا'
            }).then((result) => {
                if (result.isConfirmed) {
                    form.submit();
                }
            });
        }); // end delete btn

        // check_all
        $('#check_all').on('click', function () {
            if ($(this).is(':checked')) {
                $('.check_item').each(function () {
                    $(this).prop('checked', true)
                });

                if ($('input[class="check_item"]:checked').length) {
                    $('#delete_all_btn').removeClass('d-none');
                }

            } else {
                $('.check_item').each(function () {
                    $(this).prop('checked', false);
                    $('#delete_all_btn').addClass('d-none');
                });
            }
        });
        // end check_all


        // check checkboxBtn
           $(document).on('click','.check_item',function (){

               let total=0 ;
               $('.check_item').each(function (){
                if ($(this).is(':checked')){
                    total ++;
                }
               })

             if(total){
                 $('#delete_all_btn').removeClass('d-none');
             }else{
                 $('#delete_all_btn').addClass('d-none');
             }


           });
        // end checkboxBtn

        // delete_all_btn
        $('#delete_all_btn').on('click', function (e) {
            e.preventDefault()
            Swal.fire({
                title: 'هل أنت متأكد',
                text: "",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'نعم',
                cancelButtonText: 'لا'
            }).then((result) => {
                if (result.isConfirmed) {
                    $('#bulk_delete').submit();
                }
            });
        })
        // end delete_all_btn

    });
</script>

<script>
    $(document).ready(function () {
        $(".set > a").on("click", function () {
            if ($(this).hasClass("active")) {
                $(this).removeClass("active");
                $(this)
                    .siblings(".box-custom")
                    .slideUp(200);
                $(".set > a i")
                    .removeClass("fa-minus")
                    .addClass("fa-plus");
            } else {
                $(".set > a i")
                    .removeClass("fa-minus")
                    .addClass("fa-plus");
                $(this)
                    .find("i")
                    .removeClass("fa-plus")
                    .addClass("fa-minus");
                $(".set > a").removeClass("active");
                $(this).addClass("active");
                $(".content").slideUp(200);
                $(this)
                    .siblings(".box-custom")
                    .slideDown(200);
            }
        });
    });
</script>



