<link rel="icon" href="{{asset('assets/dashboard/img/logo.svg')}}" type="image/x-icon">
<title>Dashboard</title>
<link id="style" href="{{asset('assets/dashboard')}}/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="{{asset('assets/dashboard')}}/plugins/web-fonts/icons.css" rel="stylesheet">
<link href="{{asset('assets/dashboard')}}/plugins/web-fonts/font-awesome/font-awesome.min.css" rel="stylesheet">
<link href="{{asset('assets/dashboard')}}/plugins/web-fonts/plugin.css" rel="stylesheet">
<link href="{{asset('assets/dashboard')}}/plugins/dataTable/dataTables.bootstrap5.css" rel="stylesheet">
<link href="{{asset('assets/dashboard')}}/plugins/dataTable/Buttons/css/buttons.bootstrap5.css" rel="stylesheet">
<link href="{{asset('assets/dashboard')}}/plugins/uploader/image-uploader.css" rel="stylesheet">
<link href="{{asset('assets/dashboard')}}/plugins/jstree/css/jstree.css" rel="stylesheet">
<link href="{{asset('assets/dashboard')}}/plugins/select2/css/select2.min.css" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/dashboard')}}/plugins/multipleselect/multiple-select.css">
<link rel="stylesheet" href="{{asset('assets/dashboard')}}/plugins/magnific-popup/magnific-popup.css">
<link rel="stylesheet" href="{{asset('assets/dashboard')}}/plugins/steps/jquery-steps.min.css">
<link rel="stylesheet" href="{{asset('assets/dashboard')}}/plugins/daterangepicker/daterangepicker.css">
<link href="{{asset('assets/dashboard')}}/css/style.css?v=2" rel="stylesheet">
<link href="{{asset('assets/dashboard')}}/css/skins.css" rel="stylesheet">
<link href="{{asset('assets/dashboard')}}/css/colors/default.css" rel="stylesheet">
<link href="{{asset('assets/dashboard')}}/css/colors/color2.css" rel="stylesheet">
<link rel="stylesheet" href="{{asset('assets/dashboard/plugins/toastr/toastr.css')}}">
@toastr_css
