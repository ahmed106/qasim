@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">

                <form action="{{route('pages.update',$page->id)}}" method="post" enctype="multipart/form-data">
                    @csrf
                    @method('put')
                    <div class="card">
                        <div class="card-header">
                            <h3>تعديل الصفحه</h3>
                        </div>
                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{$page->name}}" type="text" name="name" class="form-control">
                                        <span class="input-span">الاسم</span>
                                    </div>
                                </div>


                            </div>




                        </div>
                        <div class="card-footer">
                            <button class="btn ripple btn-primary" type="submit"><i class="fe fe-save"></i> حفظ</button>
                        </div>
                    </div>
                </form>

            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->
@endsection
