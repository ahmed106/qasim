@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">
                <div class="card">
                    <div class="card-header">
                        <h3>الصفحات</h3>
                        <div class="flex-btns">

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">

                                <table class="table datatable table-striped table-bordered">
                                    <thead>
                                    <tr>

                                        <th>#</th>
                                        <th>الاسم</th>

                                        <th>العمليات</th>
                                    </tr>
                                    </thead>


                                    <tbody>


                                    </tbody>

                                </table>

                        </div>
                    </div>
                </div>
            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->

@endsection
@push('js')
    <script>
        var table = $('.table.datatable').DataTable({
            lengthChange: true,

            "processing": true,
            "serverSide": true,
            "ajax": {
                url: '{{route('pages.data')}}',
            },
            "columns": [

                {data: 'id', name: 'id'},
                {data: 'name', name: 'name'},
                {data: 'actions', name: 'actions'}
            ]


        });

    </script>
@endpush
