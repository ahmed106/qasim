@extends('dashboard.layouts.master')
@section('content')
    <div class="main-content side-content">
        <div class="container-fluid">
            <div class="inner-body">

                <form action="javascript:void();" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="card">
                        <div class="card-header">
                            <h3></h3>
                        </div>
                        <div class="card-body pb-0">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{$contact_us->name}}" readonly type="text" name="name" class="form-control">
                                        <span class="input-span">الاسم</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{$contact_us->phone}}" readonly type="text" name="name" class="form-control">
                                        <span class="input-span">الهاتف</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <input value="{{$contact_us->email}}" readonly type="text" name="name" class="form-control">
                                        <span class="input-span">البريد الالكتروني</span>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-custom">
                                        <textarea name="contact_us" class="editor" readonly id="" cols="30" rows="10">{{$contact_us->body}}</textarea>
                                        <span class="input-span">الرساله</span>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </form>

            </div>
        </div> <!-- End Main Content-->
    </div> <!-- End Page -->
@endsection
