<?php

use Illuminate\Database\Seeder;

class SectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $section = new \App\Models\Section();
        $section->create([
            'section_name'=>'home_service_section',
            'title'=>'نحن نقدم خدمات تجديد ',
            'content'=>'يشرح المُنشئ كيف يمكنك الاستمتاع باتجاهات الأرضيات الراقية مثل الخشب المنسوج والأرضيات المصفحة الجديدة. كمقاول عام',
        ]);

        $section->create([
            'section_name'=>'home_vision_section',
            'title'=>'نحن نحول الرؤية إلى قيمة فائقة',
            'content'=>'نحن نحول الرؤية إلى قيمة فائقة
معرفة كيفية العمل والتواصل مع العملاء وأصحاب المصلحة الرئيسيين هو فن ، و بيلد تاب هو الأفضل في ذلك. مجموعتنا من الناس هم الخبراء والمتفائلون للغاية لتحويل رؤية عملائنا إلى حقيقة واقعة.',
        ]);

    }
}
